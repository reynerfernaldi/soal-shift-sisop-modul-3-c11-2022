# Soal-Shift-Sisop-Modul-3-C11-2022

## Daftar Isi ##
- [Daftar Anggota Kelompok](#daftar-anggota-kelompok)
- [Nomer 3](#nomer-3)
    - [Soal  3.A](#soal-3a)
    - [Soal  3.B](#soal-3b)
    - [Soal  3.C](#soal-3c)
- [Kesulitan](#kesulitan)


## Daftar Anggota Kelompok ##

NRP | NAMA | KELAS
--- | --- | ---
5025201094  | Reyner Fernaldi | SISOP C

## Nomer 3 ##
### Soal 3.a ###

Untuk membaca file secara rekursif, saya menggunakan fungsi:
```c
void listFilesRecursively(char *basePath)
{
    char path[1000];
    struct dirent *dp;
    struct stat buffer;
    DIR *dir = opendir(basePath);
    int n = 0;

    if (!dir)
        return;

    while ((dp = readdir(dir)) != NULL)
    {
        if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0)
        {
            // Construct new path from our base path
            strcpy(path, basePath);	
            strcat(path, "/");
            strcat(path, dp->d_name);
			printf("%s\n", path);

            ...

            listFilesRecursively(path);
        }
    }
    closedir(dir);
}
```
Untuk mengkategorikannya, saya menggunakan bantuan `strrchr` dan `strtok` untuk mengambil dan memisahkan ekstensi dari filenya. Setelah didapat ekstensi filenya, saya dapat membuat folder berdasarkan ekstensi, dan memindahkan file asli ke folder ekstensi dengan menggunakan fungsi `rename(old,new)`. Semua itu saya masukkan kedalam fungsi move
```c
void *move(void *filename)
{
    char cwd[PATH_MAX];
    char dirname[200], hidden[100], hiddenname[100], file[100], existsfile[100];
    int i;
    strcpy(existsfile, filename);
    strcpy(hiddenname, filename);
    char *nama_file = strrchr(hiddenname, '/');
    strcpy(hidden, nama_file);

    //file yang hidden berawalan . adalah hidden
    if (hidden[1] == '.')
    {
        strcpy(dirname, "Hidden");
    }
    //File biasa
    else if (strstr(filename, ".") != NULL)
    {
        strcpy(file, filename);
        strtok(file, "."); //.jpg
        char *token = strtok(NULL, ""); //jpg
        //nggak case sensitive
        for (i = 0; token[i]; i++)
        {
            token[i] = tolower(token[i]);
        }
        strcpy(dirname, token);
    }
    //file gaada extensi
    else
    {
        strcpy(dirname, "Unknown");
    }

    //cek file,kalo belum ada dibuat folder
    if (file_exists(existsfile)){
        	mkdir(dirname, 0755);
	}
    //memindahkan file
    if (getcwd(cwd, sizeof(cwd)) != NULL)
    {
        char *nama = strrchr(filename, '/');
        char namafile[200];
        
        strcpy(namafile, "/home/reyner/shift3/hartakarun/");
        strcat(namafile, dirname);
        strcat(namafile, nama);

        //move file pake rename
        rename(filename, namafile);
    }
}
```
karena tida case sensitive, maka semua kategori akan di lowercase dengan bantuan fungsi `tolower`
### Soal 3.b ###
Untuk file hidden selalu diawali degan ".", sehingga kita dapat membuat if else seperti berikut:
```c
    if (hidden[1] == '.')
    {
        strcpy(dirname, "Hidden");
    }
    //File biasa
    else if (strstr(filename, ".") != NULL)
    {
        strcpy(file, filename);
        strtok(file, "."); //.jpg
        char *token = strtok(NULL, ""); //jpg
        //nggak case sensitive
        for (i = 0; token[i]; i++)
        {
            token[i] = tolower(token[i]);
        }
        strcpy(dirname, token);
    }
    //file gaada extensi
    else
    {
        strcpy(dirname, "Unknown");
    }
```
### Soal 3.c ###
Pada fungsi `listFilesRecursively` , saya membuat thread baru, sehingga tiap file yang diakses akan memiliki thread sendiri
```c
void listFilesRecursively(char *basePath)
{
    ...
            if (stat(path, &buffer) == 0 && S_ISREG(buffer.st_mode))
            {
                //membuat thread untuk cek move
                pthread_t thread;
                int err = pthread_create(&thread, NULL, move, (void *)path);
                pthread_join(thread, NULL);
            }
    ...
}

```
![Untitled](/uploads/f5e83a166f42d5dc56f85e89855777d5/Untitled.png)

## Kesulitan ##
folder yang baru (sesuai dengan ekstensi) bercampur dengan folder yang lama, sehingga kurang rapih

