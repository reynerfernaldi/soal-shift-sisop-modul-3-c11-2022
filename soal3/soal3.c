#include <stdio.h>
#include <string.h>
#include <pthread.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <ctype.h>
#include <unistd.h>
#include <stdbool.h> 

//cek file ada apa engga

bool file_exists (char *filename) {
  struct stat buffer;   
  return (stat (filename, &buffer) == 0);
}

//fungsi move
void *move(void *filename)
{
    char cwd[PATH_MAX];
    char dirname[200], hidden[100], hiddenname[100], file[100], existsfile[100];
    int i;
    strcpy(existsfile, filename);
    strcpy(hiddenname, filename);
    char *nama_file = strrchr(hiddenname, '/');
    strcpy(hidden, nama_file);

    //file yang hidden berawalan . adalah hidden
    if (hidden[1] == '.')
    {
        strcpy(dirname, "Hidden");
    }
    //File biasa
    else if (strstr(filename, ".") != NULL)
    {
        strcpy(file, filename);
        strtok(file, "."); //.jpg
        char *token = strtok(NULL, ""); //jpg
        //nggak case sensitive
        for (i = 0; token[i]; i++)
        {
            token[i] = tolower(token[i]);
        }
        strcpy(dirname, token);
    }
    //file gaada extensi
    else
    {
        strcpy(dirname, "Unknown");
    }

    //cek file,kalo belum ada dibuat folder
    if (file_exists(existsfile)){
        	mkdir(dirname, 0755);
	}
    //memindahkan file
    if (getcwd(cwd, sizeof(cwd)) != NULL)
    {
        char *nama = strrchr(filename, '/');
        char namafile[200];
        
        strcpy(namafile, "/home/reyner/shift3/hartakarun/");
        strcat(namafile, dirname);
        strcat(namafile, nama);

        //move file pake rename
        rename(filename, namafile);
    }
}

//rekursif
void listFilesRecursively(char *basePath)
{
    char path[1000];
    struct dirent *dp;
    struct stat buffer;
    DIR *dir = opendir(basePath);
    int n = 0;

    if (!dir)
        return;

    while ((dp = readdir(dir)) != NULL)
    {
        if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0)
        {
            // Construct new path from our base path
            strcpy(path, basePath);	
            strcat(path, "/");
            strcat(path, dp->d_name);
			printf("%s\n", path);

            if (stat(path, &buffer) == 0 && S_ISREG(buffer.st_mode))
            {
                //membuat thread untuk cek move
                pthread_t thread;
                int err = pthread_create(&thread, NULL, move, (void *)path);
                pthread_join(thread, NULL);
            }

            listFilesRecursively(path);
        }
    }
    closedir(dir);
}

//fungsi main
int main(int argc, char *argv[])
{
    listFilesRecursively("/home/reyner/shift3/hartakarun/");
    struct stat buffer;
    int err = stat("/home/reyner/shift3/hartakarun/", &buffer);
    if (err == -1)
    {
        printf("gagal disimpan \n");
    }
    else
    {
        printf("sukses disimpan!\n");
    }
}

